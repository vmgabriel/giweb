---
layout: page
title: FrontEnd
permalink: /front/
---

### Presentacion de Grupo
 - [Presentacion del Grupo](https://vmgabriel.gitlab.io/giweb/documentos/0-PresentacionGrupo.pdf "Presentacion de Grupo")

### Temas Y Metodologia
 - [Temaria](https://vmgabriel.gitlab.io/giweb/documentos/0-Temario.pdf "Temas de Grupo")
 - [Metodologia](https://vmgabriel.gitlab.io/giweb/documentos/0-metodologia.pdf "Metodologia de Grupo")

### Sesion 1
 - [Sesion Previa](https://vmgabriel.gitlab.io/giweb/documentos/1-sesion-previa-git.pdf "Sesion Previa")
 - [Control de Versiones](https://vmgabriel.gitlab.io/giweb/documentos/1-control-versiones.pdf "Control de Versiones")
 - [Git](https://vmgabriel.gitlab.io/giweb/documentos/1-git.pdf "GIT")
 - [Markdown](https://vmgabriel.gitlab.io/giweb/documentos/1-markdown.pdf "Markdown")
 - [Laboratorio 1](https://vmgabriel.gitlab.io/giweb/documentos/1-Laboratorio.pdf "Laboratorio 1")

### Sesion 2
 - [Sesion Previa](https://vmgabriel.gitlab.io/giweb/documentos/2-sesion-previa-html.pdf "Sesion Previa")
 - [HTML Teoria](https://vmgabriel.gitlab.io/giweb/documentos/2-HTML.pdf "Teoria de HTML")
 - [HTML Guia](https://vmgabriel.gitlab.io/giweb/documentos/2-HTML5_1_.pdf "Etiquetas y guia de ayuda")
 - [CSS](https://vmgabriel.gitlab.io/giweb/documentos/2-Css.pdf "Estilo de Cascada")
 - [BOOTSTRAP](https://vmgabriel.gitlab.io/giweb/documentos/2-BOOTSTRAP.pdf "Framework para diseño adaptable")
 - [Laboratorio](https://vmgabriel.gitlab.io/giweb/documentos/2-Laboratorio.pdf "Laboratorio 2")

### Sesion 3
 - [Sesion Previa](https://vmgabriel.gitlab.io/giweb/documentos/3-sesion-previa-CSS_hard.pdf "Sesion Previa")
 - [Metodologias CSS](https://vmgabriel.gitlab.io/giweb/documentos/3-Metodologias_CSS.pdf "Metodologias CSS")
 - [BEM](https://vmgabriel.gitlab.io/giweb/documentos/3-BEM.pdf "BEM")
 - [SMASS](https://vmgabriel.gitlab.io/giweb/documentos/3-SMACSS.pdf "SMACSS")
 - [SMACSS Guia Oficial](https://vmgabriel.gitlab.io/giweb/documentos/3-smacss-guia-oficial.pdf "SMACSS Guia Oficial")
 - [SASS](https://vmgabriel.gitlab.io/giweb/documentos/3-SASS.pdf "SASS")
 - [Guia de PUG(JADE) y css-GRID](https://vmgabriel.gitlab.io/giweb/documentos/3-guia_de_PUG_GRID.pdf "Guia de PUG y CSS-grid")

### Sesion 4
 - [Sesion Previa](https://vmgabriel.gitlab.io/giweb/documentos/4-sesion-previa-motores.pdf "Sesion Previa")
 - [Guia de Instalacion de Tecnologias](https://vmgabriel.gitlab.io/giweb/documentos/4-tecnologias-sass-express-pug.pdf "Guia de Instalacion de Tecnologias")
 - [FRONT END](https://vmgabriel.gitlab.io/giweb/documentos/4-Front-end.pdf "FRONT END")
 - [SASS y PUG(JADE)](https://vmgabriel.gitlab.io/giweb/documentos/4-sass_y_PUG_en_servidor.pdf "SASS y JADE")
 - [Laboratorio 4](https://vmgabriel.gitlab.io/giweb/documentos/4-Laboratorio.pdf "Laboratorio 4")

### Sesion 5
 - [Sesion Previa](https://vmgabriel.gitlab.io/giweb/documentos/5-sesion-previa-Maquetacion.pdf "Sesion Previa")

# Universidad Distrital Francisco José de Caldas
Las licencias de cada una de las cosas es de cada uno de los respectivos dueños, agradezco la atencion prestada
