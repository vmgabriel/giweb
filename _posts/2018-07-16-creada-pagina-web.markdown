---
layout: post
title:  "Completada la Pagina Principal de GIWEB"
date:   2018-07-16 17:00:00 -0500
categories: pagina web
miniatura: 2018-07-16-completado-web.png
description: "Esta Completado el inicio de la pagina web del capitulo estudiantil GIWEB de la universidad Distrital Francisco José de Caldas, siempre hemos tenido las ganas de tener una pagina especificamente para dar noticas y publicar una que otra cosa."
---
Hola de nuevo, esta publicacion se hace en nombre de la pagina, ya ahora disponible y funcional, se estarán publicando contenido del mismo, especialmente obteniendo en el todo lo que recurra a la documentacion y entre otra cosas todo el contenido en si, lo que lo hace completamente util al grupo y esto genera todo una gran gama de utilidades para el grupo.

Estamos atentos ante cualquier cambio.
